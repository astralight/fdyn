﻿using UnityEngine;
using System.Collections;
using CIM = CustomInput.Manager;
using UnityEngine.EventSystems;
using UEI = UnityEngine.Input;
using UserInterface;
using UnityEngine.SceneManagement;

namespace Player
{
	public class Controller : MonoBehaviour
	{
		[SerializeField]
		float HLOOKSPEED, VLOOKSPEED, HLOOKCLAMP, VLOOKCLAMP, HMOVESPEED, VMOVESPEED, HMOVECLAMP, VMOVECLAMP;
		public void Restart()
		{ SceneManager.LoadScene(0); }

		// Use this for initialization
		void Start()
		{
			StartCoroutine(Move());
			StartCoroutine(View());
			StartCoroutine(Command());
		}
		[SerializeField]
		Game.PipeBag pipeBag;
		[SerializeField]
		new Camera camera;
		IEnumerator Move()
		{
			while (true)
			{
                #region circular
                //transform.parent.localEulerAngles += -transform.parent.up * modifyClampValue(Knob.GetKnobType(Knob.KnobType.Movement).x, HMOVECLAMP, HMOVESPEED);
                //transform.position += Vector3.up * modifyClampValue(Knob.GetKnobType(Knob.KnobType.Movement).y, VMOVECLAMP, VMOVESPEED);
                #endregion
                #region fps
                transform.position -= transform.forward * modifyClampValue(Knob.GetKnobType(Knob.KnobType.Movement).x, HMOVECLAMP, HMOVESPEED);
                transform.position += camera.transform.forward * modifyClampValue(Knob.GetKnobType(Knob.KnobType.Movement).y, VMOVECLAMP, VMOVESPEED);
                #endregion
                Vector3 v3 = transform.position;
				if (v3.y < 1) v3.y = 1;
				if (v3.y > 10) v3.y = 10;
                if ((v3 - Vector3.up).magnitude > 10)
                    v3 = (v3-Vector3.up).normalized*10;
				transform.position = v3;
				yield return null;
			}
		}
		[SerializeField]
		Vector2 VLookRange = new Vector2(-60, 45);
		IEnumerator View()
		{
			Knob k = Knob.GetKnobType(Knob.KnobType.View);
            while (true)
			{
				if (k.clicked)
					transform.localEulerAngles = camera.transform.localEulerAngles = Vector3.zero;
				/*
				Vector3 v3 = camera.transform.localEulerAngles;
				v3 += (Vector3.left * Knob.GetKnobType(Knob.KnobType.View).y / 4f);
				if (v3.x > 180) v3 -= Vector3.right * 360; // stop the wrapping, allow v3.x to be negative.
				if (v3.x < LookRange[0]) v3.x = LookRange[0];
				if (v3.x > LookRange[1]) v3.x = LookRange[1];
				camera.transform.localEulerAngles = v3;
				*/
				transform.localEulerAngles += transform.up * modifyClampValue(k.x, HLOOKCLAMP, HLOOKSPEED);
				camera.transform.localEulerAngles += Vector3.left * modifyClampValue(k.y, VLOOKCLAMP, VLOOKSPEED);
				Vector3 v3 = camera.transform.localEulerAngles;
				if (v3.x > 180) v3 -= Vector3.right * 360; // stop the wrapping, allow v3.x to be negative.
				if (v3.x < VLookRange[0]) v3.x = VLookRange[0];
				if (v3.x > VLookRange[1]) v3.x = VLookRange[1];
				camera.transform.localEulerAngles = v3;
				//camera.transform.localEulerAngles += Vector3.right * Mathf.Clamp((VLOOKSPEED * Knob.GetKnobType(Knob.KnobType.View).y), -VLOOKRANGE, VLOOKRANGE);
				yield return null;
			}
		}

		float modifyClampValue(float v, float clampValue, float multiplier)
		{
			v = Mathf.Sqrt(Mathf.Abs(v))*Mathf.Sign(v);
			if (Mathf.Abs(v) < clampValue * 0.4f) return 0;
			v -= clampValue * 0.4f * Mathf.Sign(v);
			v = v * multiplier;
			v = Mathf.Clamp(v, -clampValue, clampValue);
			return v;
		}
		IEnumerator Command()
		{
			while(true)
			{
				Transform t;
				if(CIM.Command(out t))
				{
					Game.Connection c = t.GetComponent<Game.Connection>();
					if(c.open && !pipeBag.isWorking)
	                    pipeBag.GetNextPipe(c);
				}
				yield return null;
			}
		}
	}

}
namespace CustomInput
{
	class Manager
	{
		public static bool Command(out Transform connection)
		{
			connection = null;
#if UNITY_IPHONE
			if (UnityEngine.Input.touchCount > 0)
			{
				Touch t = UnityEngine.Input.GetTouch(0);
				if (EventSystem.current == null || !EventSystem.current.IsPointerOverGameObject())
				{
					RaycastHit rh;
					if (Physics.Raycast(Camera.main.ScreenPointToRay(UEI.mousePosition), out rh, 100, 1 << 8))
					{
						Debug.Log("1");
					}
				}
			}
#elif UNITY_ANDROID
			{
				foreach (Touch t in UnityEngine.Input.touches)
				{
					if (t.phase != TouchPhase.Began) continue;
					//if (EventSystem.current == null || !EventSystem.current.IsPointerOverGameObject())
					{
						RaycastHit rh;
						if (Physics.Raycast(Camera.main.ScreenPointToRay(t.position), out rh, 100, 1 << 8))
						{
							connection = rh.transform;
							return true;
						}
					}
				}
			}
#else
			if (UEI.GetMouseButtonDown(0))
			{
				if(EventSystem.current == null || !EventSystem.current.IsPointerOverGameObject())
				{
					RaycastHit rh;
					if(Physics.Raycast(Camera.main.ScreenPointToRay(UEI.mousePosition),  out rh, 100, 1<<8))
					{
						connection = rh.transform;
						return true;
					}
				}
			}
			
#endif
			return false;
		}

		public static bool GetMouseOrTouch(out Vector3 v3)
		{
			bool ret = false;
			v3 = Vector3.zero;
#if UNITY_IPHONE
			return UEI.touchCount > 0 && (v3 = UEI.touches[0].position) != null;
#elif UNITY_ANDROID
			for (int i = 0; i < Input.touchCount; ++i)
			{
				if (Input.GetTouch(i).phase == TouchPhase.Began)
				{
					ret = true;
					v3 = Input.GetTouch(i).position;
				}
			}
			return ret;
#else
			v3 = UEI.mousePosition;
			return true;
#endif
		}
		public static bool GetMouseOrTouchStay(out Vector3 v3)
		{
			bool ret = false;
			v3 = Vector3.zero;
#if UNITY_IPHONE
			return UEI.touchCount > 0 && (v3 = UEI.touches[0].position) != null;
#elif UNITY_ANDROID
			for (int i = 0; i < Input.touchCount; ++i)
			{
				if ((Input.GetTouch(i).phase == TouchPhase.Began) || (Input.GetTouch(i).phase == TouchPhase.Moved) || (Input.GetTouch(i).phase == TouchPhase.Stationary))
				{
					ret = true;
					v3 = Input.GetTouch(i).position;
				}
			}
			return ret;
#else
			v3 = UEI.mousePosition;
			return Input.GetMouseButton(0);
#endif
		}

		static string console = "";
		void OnGUI()
		{
			GUILayout.FlexibleSpace();
			GUILayout.Box(console);
		}

	}
}
