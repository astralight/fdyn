﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
namespace UserInterface
{
	public class UIBase : MonoBehaviour
	{
		RectTransform _rt;
		public RectTransform rt { get { return _rt ?? (_rt = GetComponent<RectTransform>()); } }
	}

	public class Knob : UIBase {
		static Dictionary<KnobType, Knob> Knobs = new Dictionary<KnobType, Knob>();
		public static Knob GetKnobType(KnobType kt) { return Knobs.ContainsKey(kt) ? Knobs[kt] : null; }
		public enum KnobType { Movement, View }

		[SerializeField] KnobType ktype;

		protected float KNOB_RETURN_SPEED { get { return 10f; } }
		protected float KNOB_DRAG_SPEED { get { return 0.5f; } }
		protected float KNOB_MAX_DISTANCE{ get { return 35; } }

		void Awake()
		{
			Register();
		}

		void Register()
		{
			Knobs[ktype] = this;
		}
		// Use this for initialization
		protected void Start () {
			StartCoroutine(KeepAnchored());
		}

		protected IEnumerator KeepAnchored () {
			while(true)
			{
				if (!beingDragged)
				{
					if (rt.anchoredPosition.sqrMagnitude > 0)
					{
						rt.anchoredPosition = Vector3.MoveTowards(rt.anchoredPosition, Vector3.zero, KNOB_RETURN_SPEED);
						currentDelta = Vector3.zero;
					}
				}
				yield return null;
			}
		}
		protected bool beingDragged = false;
		protected Vector3 downPosition;
		public string console = "";
		public void OnGUI()
		{
			//GUILayout.Box(console);
			if (console.Length > 1000)
				console = "";
		}

		private bool _clicked = false;
		public bool clicked { get { return _clicked && !(_clicked = !_clicked); } }
		private System.DateTime downTime;

		protected void OnDown()
		{
			downTime = System.DateTime.Now;
			beingDragged = true;
			bool b = CustomInput.Manager.GetMouseOrTouch(out downPosition);
			console += "ON DOWN"+downPosition + b +"\n";
		}
		protected void OnDrag()
		{
			Vector3 position;
			bool b = CustomInput.Manager.GetMouseOrTouchStay(out position);
			Vector3 delta = (position - downPosition) * KNOB_DRAG_SPEED;
			if (delta.magnitude > KNOB_MAX_DISTANCE)
				delta = delta.normalized * KNOB_MAX_DISTANCE;
            rt.anchoredPosition = delta;
			currentDelta = delta;
			console += "ON DRAG" + position + b + "\n";
		}
		protected void OnUp()
		{
			beingDragged = false;
			console += "ON UP\n";
		}

		public Vector3 currentDelta { get; protected set; }
		public float x { get { return (Mathf.Abs(currentDelta.x)) * Mathf.Sign(currentDelta.x); } }
		public float y { get { return (Mathf.Abs(currentDelta.y)) * Mathf.Sign(currentDelta.y); } }
	}

}
