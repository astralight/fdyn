﻿using UnityEngine;
using System.Collections;
namespace Game
{
	public class Connection : MonoBehaviour {
		protected bool _open = true;
		public bool open {
			get
			{
				return _open;
			}
			set
			{
				_open = GetComponent<Renderer>().enabled = value;
			}
		}
	}
}
