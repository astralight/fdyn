﻿using UnityEngine;
using System.Collections;
namespace Game
{
	public class Emitters : MonoBehaviour {
		public void OnDone()
		{
			foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
				ps.startLifetime = 0;
			foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
				ps.startLifetime = 10;
			foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
				ps.Emit(1000);
		}
	}
}
