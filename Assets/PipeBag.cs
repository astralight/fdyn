﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Game
{
	public class PipeBag : MonoBehaviour {
        int PipeIndex = 0;
		public GameObject nextPipe { get { return pipes[PipeIndex]; } }
        [SerializeField] List<GameObject> pipes;
        [SerializeField] List<UnityEngine.UI.Button> buttons;
        public void SetPipe(int index)
        {
            Debug.Log(index);
            PipeIndex = index;
            for (int i = 0; i < 4; i++)
                buttons[i].GetComponent<UnityEngine.UI.RawImage>().color = index == i ? Color.white : Color.gray;
        }
		public GameObject GetNextPipe(Connection connection)
		{
			GameObject go = GameObject.Instantiate(nextPipe);
			StartCoroutine(Connect(go.transform.GetComponentsInChildren<Connection>(), connection));
			return go;
		}
        void Start()
        {
            SetPipe(0);
        }
		public bool isWorking = false;
		//T1 is the connection you are placing ONTO T2.
		public IEnumerator Connect(Connection[] cs, Connection c2)
		{
			isWorking = true;
			Connection c1 = cs[0];
			int i = 0;

			while (true)
			{
				c1 = cs[i];
				//c1.transform.parent.transform.Rotate(-c2.transform.eulerAngles - c1.transform.eulerAngles);
				GameObject g = new GameObject();
				Transform pivot = g.transform;
				pivot.parent = c1.transform;
				pivot.localEulerAngles = Vector3.zero;
				pivot.localPosition = Vector3.zero;
				pivot.localScale = Vector3.one;
				pivot.parent = null;
				c1.transform.parent.parent = pivot;
				pivot.parent = c2.transform;
				pivot.localPosition = Vector3.zero;
				pivot.localEulerAngles = Vector3.up * 180;
				c1.transform.parent.parent = c2.transform.parent.parent;
				Destroy(g);
				//c1.transform.parent.transform.position += c2.transform.position - c1.transform.position;
				bool connectionPointMatters = false;
				if (connectionPointMatters) //Only do this code if the connection point matters (for example, Y matters, X doesn't)
				{
					while (!Input.GetKeyDown(KeyCode.Return) && !Input.GetKeyDown(KeyCode.Space))
					{
						Debug.Log("CONNECT " + c1 + " " + +i + " " + Time.time);
						yield return null;
					}
					if (Input.GetKeyDown(KeyCode.Return))
					{
						break;
					}
					else if (Input.GetKeyDown(KeyCode.Space))
					{
						yield return null;
						i++; i %= cs.Length;
						continue;
					}
					yield return null;
				}
				else
				{
					break;
				}
			}
			c1.open = c2.open = false;
			yield return null;
			/*
			while (true)
			{
				while (!Input.GetKeyDown(KeyCode.Return) && !Input.GetKeyDown(KeyCode.Space))
				{
					Debug.Log("TURN "+c1+" "+i + " " + Time.time);
					yield return null;
				}
				if (Input.GetKeyDown(KeyCode.Return))
				{
					break;
				}
				else if (Input.GetKeyDown(KeyCode.Space))
				{
					c1.transform.parent.Rotate(c2.transform.right*90, Space.World);
					yield return null;
					i++; i %= cs.Length;
					continue;
				}
				yield return null;
			}
			*/
			Vector3? first = null;
            Vector3 v3;
			while(CustomInput.Manager.GetMouseOrTouchStay(out v3))
			{
				if (first == null) first = v3;
				Vector3 difference = (Vector3)first - v3;
				float difX = difference.x;
				difX = Mathf.Round(difX / 200) * 90;
				if (Mathf.Abs(difX) > 1) first = v3;
				c1.transform.parent.Rotate(c2.transform.right * difX, Space.World);
				yield return null;
			}
				Debug.Log(v3);

			yield return null;
			isWorking = false;
		}
	}
}
