﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatchNotes : MonoBehaviour {
	[SerializeField]
	string VersionID;
	[SerializeField]
	System.DateTime Time;
	[SerializeField]
	List<string> Fixes;
	[SerializeField]
	List<string> KnownBugs;
}
